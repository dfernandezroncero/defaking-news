from flask import Flask, jsonify, request as req
from flask_cors import CORS
import pandas as pd
import json
from pandas import DataFrame
from new_algoritmo_BBDD import *
import json

app = Flask(__name__)
CORS(app)

@app.route("/")
def hello():
    return 'Hello Fake News Predictor'

@app.route("/cargaBBDD/", methods=["GET"]) #anotaciones en Flask, información que se pasa a la funsión
def calculoEntrenamiento():
    CalculoEntrenamiento()
    return "OK"
    

@app.route("/prediction/", methods=["POST"])
def recomendacionUsuario1():
    content_client= str(req.args.get('title'))
    content_client= content_client+ " " +str(req.args.get('content'))
    #content_client= content_client+ " " +str(req.args.get('domain'))
    #news={'title':title,'content':content}
    #df_news_cliente = DataFrame(news)
    #El resultado es: {"Prediction":{"Doc1":"reliable"}}
    recomendacion=CalculoProbabilidades(content_client)
    
    return recomendacion

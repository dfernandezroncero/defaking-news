import pandas as pd
#Leemos datos
import os
import io
import numpy
from pandas import DataFrame
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from ConexionDDBB import *


classifier = MultinomialNB()
vectorizer = CountVectorizer()

def CalculoEntrenamiento():
    #real_datos = pd.read_csv('C:\GIT\DeFaking News\defaking-news\\Datos\\datos_reliable.csv')
    #real_datos = pd.read_csv('C:\GIT\DeFaking News\defaking-news\\Datos\\datos_reliable.csv')
    #real_datos = pd.read_csv('C:\\Users\\kike\\Desktop\\DefakingNews\\defaking-news\\Datos\\datos_reliable.csv')
    real_datos =selectNews('real')
    #real_title=real_datos.drop(columns=['Unnamed: 0','Unnamed: 0.1','id','url'])
    #rating=rating[rating['rating']!=-1] eliminar valores que no nos interesa
    #fake_datos = pd.read_csv('C:\GIT\DeFaking News\defaking-news\\Datos\\datos_fake.csv')
    #fake_datos = pd.read_csv('C:\\Users\\kike\\Desktop\\DefakingNews\\defaking-news\\Datos\\datos_fake.csv')
    fake_datos =selectNews('fake')
    #fake_title=fake_datos.drop(columns=['Unnamed: 0','Unnamed: 0.1','id','url'])
    print(real_datos.head())
    print(fake_datos.head())

    real_title = real_datos[:60000]
    fake_title = fake_datos[:44000]

    real_title['type'] = 'RELIABLE'
    fake_title['type'] = 'FAKE'

    
    df_new = pd.concat([fake_title, real_title])
    #Calculo de la Clase Real

    print(df_new.head())
    
    #print(tam_listas_real+tam_listas_fake)
    #Pasamos los strings a lista

    
    counts = vectorizer.fit_transform(df_new['content'].values+" "+df_new['dominio'].values)
    #Palabras y su frecuencia en cada documento
    #print(len(counts))
    #El vocabulario
    #print(vectorizer.vocabulary_.items())
    targets = df_new['type'].values
    classifier.fit(counts, targets)
    
def CalculoProbabilidades(content_client):
    
    examples=[str(content_client)]
    example_counts = vectorizer.transform(examples)
    predictions = classifier.predict(example_counts)
    predictions_porcent = classifier.predict_proba(example_counts)

    print(predictions_porcent[0][0])
    print(predictions_porcent[0][1])

    resultado = ""
    if predictions_porcent[0][0] > predictions_porcent[0][1]:
        resultado = str(round((predictions_porcent[0][0]*100)/(predictions_porcent[0][0] + predictions_porcent[0][1]),2)) + "% chance of being " + str(predictions[0])
    elif predictions_porcent[0][0] < predictions_porcent[0][1]:
        resultado = str(round((predictions_porcent[0][1]*100)/(predictions_porcent[0][0] + predictions_porcent[0][1]),2)) + "% chance of being " + str(predictions[0])
    else:
        resultado = "Unable to determine at the moment ¯\_(ツ)_/¯ \n Algorithm Optimization In Progress"

    
    #predictions_df=pd.DataFrame(predictions, columns = ['Prediction','Percent'], index=['Doc1'])
    #print(predictions_df)
    

    return resultado

  

    '''real =[]
    fake = []
    
    cont = 0
    for i in predictions:

        if cont<(len(real_title)):
            real.append(i)
        else:
            fake.append(i)
        cont += 1

    print("REALES!!!")

    suma_real = 0
    suma_fake = 0
    for i in real:
        if i == "reliable":
            suma_real += 1
        else:
            suma_fake += 1

    print("Contenido real ", suma_real)
    print("Contenido fake ", suma_fake)
    print("  ")
    
    print("FAKES!!!")
    suma_real = 0
    suma_fake = 0
    for i in fake:
        if i == "reliable":
            suma_real += 1
        else:
            suma_fake += 1

    print("Contenido real ", suma_real)
    print("Contenido fake ", suma_fake)'''


import pandas as pd
#Leemos datos
import os
import io
import numpy
from pandas import DataFrame
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from filtrar_datos import *



classifier = MultinomialNB()
vectorizer = CountVectorizer()

def CalculoEntrenamiento():
    #real_datos = pd.read_csv('C:\GIT\DeFaking News\defaking-news\\Datos\\datos_reliable.csv')
    #real_datos = pd.read_csv('C:\GIT\DeFaking News\defaking-news\\Datos\\datos_reliable.csv')
    #real_datos = pd.read_csv('C:\\Users\\kike\\Desktop\\DefakingNews\\defaking-news\\Datos\\datos_reliable.csv')
    real_datos = pd.read_csv('D:\\apis_python\\api3\\defaking-news\\Datos\\datos_reliable.csv')
    real_title=real_datos.drop(columns=['Unnamed: 0','Unnamed: 0.1','id','url'])

    #rating=rating[rating['rating']!=-1] eliminar valores que no nos interesa

    #fake_datos = pd.read_csv('C:\GIT\DeFaking News\defaking-news\\Datos\\datos_fake.csv')
    #fake_datos = pd.read_csv('D:\\apis_python\\api3\\defaking-news\\Datos\\datos_fake.csv')
    #fake_datos = pd.read_csv('C:\\Users\\kike\\Desktop\\DefakingNews\\defaking-news\\Datos\\datos_fake.csv')
    #fake_datos = pd.read_csv('C:\\Users\\Admin\\Desktop\\Anna Masdeu - Lenovo\\DeFaking News\\defaking-news\\Datos\\datos_fake.csv')
    fake_datos = pd.read_csv('D:\\apis_python\\api3\\defaking-news\\Datos\\datos_fake.csv')
    fake_title=fake_datos.drop(columns=['Unnamed: 0','Unnamed: 0.1','id','url'])
    print(len(real_title))
    print(len(fake_title))

    real_title = real_title[:44460]
    fake_title = fake_title[:44460]

    filtradorDatos(real_title)
    
    real_title['type'] = 'reliable'
    fake_title['type'] = 'fake'
    
    df_new = pd.concat([fake_title, real_title])
    #Calculo de la Clase Real

    print(df_new.head())
    
    #print(tam_listas_real+tam_listas_fake)
    #Pasamos los strings a lista

    
    counts = vectorizer.fit_transform(df_new['content'].values+" "+df_new['domain'].values)
    #Palabras y su frecuencia en cada documento
    print(type(counts))
    #El vocabulario
    #print(vectorizer.vocabulary_.items())
    targets = df_new['type'].values
    classifier.fit(counts, targets)
    
def CalculoProbabilidades(content_client):
    
    examples=[str(content_client)]
    example_counts = vectorizer.transform(examples)
    predictions = classifier.predict(example_counts)
    predictions_df=pd.DataFrame(predictions, columns = ['Prediction'], index=['Doc1'])
    print(predictions_df)
    

    return predictions_df.to_json()

  

    '''real =[]
    fake = []
    
    cont = 0
    for i in predictions:

        if cont<(len(real_title)):
            real.append(i)
        else:
            fake.append(i)
        cont += 1

    print("REALES!!!")

    suma_real = 0
    suma_fake = 0
    for i in real:
        if i == "reliable":
            suma_real += 1
        else:
            suma_fake += 1

    print("Contenido real ", suma_real)
    print("Contenido fake ", suma_fake)
    print("  ")
    
    print("FAKES!!!")
    suma_real = 0
    suma_fake = 0
    for i in fake:
        if i == "reliable":
            suma_real += 1
        else:
            suma_fake += 1

    print("Contenido real ", suma_real)
    print("Contenido fake ", suma_fake)'''


import mysql.connector
import pandas as pd

#crearConexion
mydb = mysql.connector.connect(
	user = "root",
	password ='new_password',
	host='localhost',
	database='news'
)

mycursor = mydb.cursor()

def multiReplace(palabra, listaRemplaces):
    for elem in listaRemplaces:
        if elem in palabra:
            palabra = palabra.replace(elem, " ", 5)

    return palabra


def filtrarTexto(texto):
        
        texto = texto.lower().split()

        lista_simbolos = ['~','“','”','‘','!','@','#','$','%','^','&','*','(',')','[',']','{','}',';',':',',','.','/','<','>','?','|','`','~','—','-','=','_','+','"', '–','…']
        lista_neutral_words = ['about','beside','near','to','above','between','of','towards','across','beyond','off','under','after','by','on','underneath','against','despite','onto','unlike','along','down','opposite','until','among','during','out','up','around','except','outside','upon','as','for','over','via','at','from','past','with','before','in','round','within','behind','inside','since','without','below','into','than','beneath','like','through', 'all','another','any','both','each','either','enough','few','fewer','less','little','many','more','most','much','neither','several','some','few','fewer','fewest','every','most','that','little','half','much','the','other','another','her','my','their','a','an','his','neither','these','all','its','no','this','any','one','two','three','those','both','least','our','what','each','less','several','which','either','many','some','whose','enough','more','such','your']
        words_temporal = ""

        for i in range(0, len(texto)):
                texto[i] = multiReplace(texto[i], lista_simbolos).replace(" ", "")
                if texto[i] not in lista_neutral_words:
                        words_temporal += texto[i] + " "
                        
        return words_temporal




#Inserta noticia en la tabla indicada (tipo). Valores es una lista de strings con la siguiente estructura (titulo, cuerpo, dominio)
def insertNew(tipo, valores):
	query = "INSERT INTO " + tipo + "_news (titulo, cuerpo, dominio) VALUES (%s, %s, %s)"	
	mycursor.execute(query,valores)
	mydb.commit()
	print(mycursor.rowcount, "record inserted.")

#valores = ("ttt", "ttt", "ttt")
#insertNew("actualizacion", valores)


#Selecciona todas las noticias de la tabla indicada(tipo) y devuelve la variable word con todos los datos en forma de string muy largo.
def selectNews(tipo):
	query = "SELECT titulo,cuerpo,dominio FROM " + tipo + "_news"
	mycursor.execute(query)
	resultado = mycursor.fetchall()

	#imprime 5 primeros titulos para ver que ha funcionado
	cont = 0
	for fila in resultado:
		print(fila[0])
		cont+=1
		if cont == 5:
			break
	lista_titulo = []
	lista_content = []
	lista_dominio = []
	#words = ""
        
	for fila in resultado:
		lista_titulo.append(filtrarTexto(fila[0]))
		lista_content.append(filtrarTexto(fila[1]))
		lista_dominio.append(fila[2])

	df = pd.DataFrame(list(zip(lista_titulo, lista_content, lista_dominio)), columns = ['title', 'content', 'dominio'])
	return df

#print(selectNews('fake'))



'''
valores = ("titulo", "cuerpo", "dominio")
insertFakeNew(valores, mycursor)
valores = ("uno", "uno", "uno")
insertFakeNew(valores, mycursor)
'''


#Borra noticia de la tabla actualizacion_news. Hay que indicar el id de la noticia como parámetro
def deleteNew(id):
	query = "DELETE FROM actualizacion_news WHERE id_actualizacion = %i" % (id)
	mycursor.execute(query)
	mydb.commit()
	print(mycursor.rowcount, "record deleted.")

#deleteNew(5)

import pandas as pd
import json
from pandas import DataFrame
from filtrar_datos import *
def PreditionFakeReal():
    #Calculo de la probabilidad del nuevo documento sea positivo o negativo:
    prueba_datos = pd.read_csv('D:\\apis_python\\api3\\defaking-news\\Datos\\News_prueba.csv',sep=';')

    #Quitamos caracteres especiales y guardamos en un string
    documento_a_evaluar=filtradorDatos(prueba_datos)
    print(len(documento_a_evaluar))
    print(documento_a_evaluar)

    #Cargamos JSON y transformamos en DataFrame:
    df_real = pd.read_json(r'D:\\apis_python\\api3\\defaking-news\\Algoritmo\\Real_Probability.json',orient='index')#DataFrame(load_real_json)
    print(df_real.head())
    lista_pares_prob_real = df_real.values.tolist()
    print(lista_pares_prob_real)

    df_fake = pd.read_json(r'D:\\apis_python\\api3\\defaking-news\\Algoritmo\\Fake_Probability.json',orient='index')
    print(df_fake.head())
    lista_pares_prob_fake = df_fake.values.tolist()
    print(lista_pares_prob_fake)
    #Calculo probabilidad de que sea positivo:
    P_real=len(lista_pares_prob_real)/(len(lista_pares_prob_real)+len(lista_pares_prob_fake)) #Probabilidad de clase real
    print(P_real)
    lista_palabras_real = []
    for palabra in lista_pares_prob_real:
        for valor in palabra:
            if type(valor) != float and valor in documento_a_evaluar:
                lista_palabras_real.append(palabra)     
    print(lista_palabras_real)                 

    prob_real_total=1
    for palabra in lista_palabras_real:
        for valor in palabra:
            print(type(valor))
            print(valor)
            if type(valor) == float:
                prob_real_total = prob_real_total*valor*100000
                print(prob_real_total)
    print(prob_real_total)             
    prob_real_total= (prob_real_total*P_real)


    print("La probabilidad de que sea Real es:", prob_real_total)

    #Calculo probabilidad de que sea negativo:
    P_fake=len(lista_pares_prob_fake)/(len(lista_pares_prob_real)+len(lista_pares_prob_fake)) #Probabilidad de clase fake
    print(P_fake)
    lista_palabras_fake = []
    for palabra in lista_pares_prob_fake:
        for valor in palabra:
            if type(valor) != float and valor in documento_a_evaluar:
                lista_palabras_fake.append(palabra)
    print(lista_palabras_fake)
    prob_fake_total=1
    for palabra in lista_palabras_fake:
        for valor in palabra:
            if type(valor) == float:
                prob_fake_total = prob_fake_total*valor*100000
    print(prob_fake_total)            
    prob_fake_total =  (prob_fake_total*P_fake)
    print("La probabilidad de que sea Fake es:", prob_fake_total)
    #Porcetaje de Fake y Real:
    procent_real=(prob_real_total/(prob_fake_total+prob_real_total))
    procent_fake=(prob_fake_total/(prob_fake_total+prob_real_total))

    #Decición de si Noticia es Fake o Real
    if prob_fake_total > prob_real_total:
        print("The News Has A High Probability Of Beging Fake: %f " %procent_fake)
    else:
        print("The News Has A High Probability Of Beging Reliable: %f" %procent_real)

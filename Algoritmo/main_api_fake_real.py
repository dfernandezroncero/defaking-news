from flask import Flask, jsonify, request as req
import pandas as pd
import json
from pandas import DataFrame
from Function_Real_Fake_Prediction import PreditionFakeReal
from filtrar_datos import *
from new_algoritmo import *
app = Flask(__name__)
@app.route("/")
def hello():
    return 'Hello Fake News Predictor'

@app.route("/cargaBBDD/", methods=["GET"]) #anotaciones en Flask, información que se pasa a la funsión
def cargaProbabilidades():
    CalculoProbabilidades()
    

@app.route("/prediction/", methods=["POST"])
def recomendacionUsuario1():
    title= req.args.get('title')
    content_client=req.args.get('content')
    #news={'title':title,'content':content}
    #df_news_cliente = DataFrame(news)
    recomendacion=CalculoProbabilidades(content_client)
    return recomendacion

import pandas as pd
import json
from pandas import DataFrame
from filtrar_datos import *
from sklearn.metrics import mean_squared_error, r2_score


'''
#Calculo de la probabilidad del nuevo documento sea positivo o negativo:
prueba_datos = pd.read_csv('C:\\Users\\kike\\Desktop\\DefakingNews\\defaking-news\\Datos\\News_prueba.csv',sep=',')

#Quitamos caracteres especiales y guardamos en un string
documento_a_evaluar=filtradorDatos(prueba_datos)
print(len(documento_a_evaluar))
#print(documento_a_evaluar)

'''
real_datos = pd.read_csv('C:\\Users\\kike\\Desktop\\DefakingNews\\defaking-news\\Datos\\datos_reliable.csv')
real_title=real_datos.drop(columns=['Unnamed: 0','Unnamed: 0.1','id','domain','url'])
#rating=rating[rating['rating']!=-1] eliminar valores que no nos interesa
fake_datos = pd.read_csv('C:\\Users\\kike\\Desktop\\DefakingNews\\defaking-news\\Datos\\datos_fake.csv')
fake_title=fake_datos.drop(columns=['Unnamed: 0','Unnamed: 0.1','id','domain','url'])

real_title = real_title[:5943]
fake_title = fake_title[:5943]


#Cargamos JSON y transformamos en DataFrame:
df_real = pd.read_json(r'C:\\Users\\kike\\Desktop\\DefakingNews\\defaking-news\\Algoritmo\\Real_Probability.json',orient='index')#DataFrame(load_real_json)
#print(df_real.head())
lista_pares_prob_real = df_real.values.tolist()
#print(lista_pares_prob_real)

df_fake = pd.read_json(r'C:\\Users\\kike\\Desktop\\DefakingNews\\defaking-news\\Algoritmo\\Fake_Probability.json',orient='index')
#print(df_fake.head())
lista_pares_prob_fake = df_fake.values.tolist()
#print(lista_pares_prob_fake)


#Calculo probabilidad de que sea positivo
totalFake = 0
totalReliable = 0

for i in range(1, len(fake_title)):
    documento_a_evaluar=filtradorDatos(fake_title.iloc[i])
    lista_palabras_real = []
    
    for palabra in lista_pares_prob_real:
        for valor in palabra:
            if type(valor) != float and valor in documento_a_evaluar:
                lista_palabras_real.append(palabra)     
    #print(lista_palabras_real)                 

    P_real=10000/(10000+10000)
    prob_real_total=1
    for palabra in lista_palabras_real:
        for valor in palabra:
            #print(type(valor))
            #print(valor)
            if type(valor) == float:
                valor = valor * 10000
                prob_real_total = prob_real_total*valor
    #print(prob_real_total)             
    prob_real_total= (prob_real_total*P_real)


    #print("La probabilidad de que sea Real es:", prob_real_total)

    #Calculo probabilidad de que sea negativo:
    P_fake=10000/(10000+10000) #Probabilidad de clase fake
    #print(P_fake)
    lista_palabras_fake = []

    for palabra in lista_pares_prob_fake:
        for valor in palabra:
            if type(valor) != float and valor in documento_a_evaluar:
                lista_palabras_fake.append(palabra)
    #print(lista_palabras_fake)

    prob_fake_total=1
    for palabra in lista_palabras_fake:
        for valor in palabra:
            if type(valor) == float:
                valor = valor * 10000
                prob_fake_total = prob_fake_total*valor
                
    #print(prob_fake_total)            
    prob_fake_total =  (prob_fake_total*P_fake)

    #Decición de si Noticia es Fake o Real
    if prob_fake_total > prob_real_total:
        totalFake += 1
        #print("The News Has A High Probability Of Beging Fake: %f " %procent_fake)
    else:
        totalReliable += 1
        #print("The News Has A High Probability Of Beging Reliable: %f" %procent_real)


print(totalFake)
print(totalReliable)

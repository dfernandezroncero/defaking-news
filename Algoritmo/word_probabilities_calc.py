import pandas as pd
from filtrar_datos import * 
#Leemos datos

def CalculoProbabilidades():
    real_datos = pd.read_csv('C:\\Users\\kike\\Desktop\\DefakingNews\\defaking-news\\Datos\\datos_reliable.csv')
    real_title=real_datos.drop(columns=['Unnamed: 0','Unnamed: 0.1','id','domain','url'])
    #rating=rating[rating['rating']!=-1] eliminar valores que no nos interesa
    fake_datos = pd.read_csv('C:\\Users\\kike\\Desktop\\DefakingNews\\defaking-news\\Datos\\datos_fake.csv')
    fake_title=fake_datos.drop(columns=['Unnamed: 0','Unnamed: 0.1','id','domain','url'])

    real_title = real_title[0:2000]
    fake_title = fake_title[0:1800]
    #Calculo de la Clase Real

    tam_listas_real = len(real_title)
    print(tam_listas_real)
    tam_listas_fake = len(fake_title)
    print(tam_listas_fake)
    #print(tam_listas_real+tam_listas_fake)
    #Pasamos los strings a lista

    lista_c_r = filtradorDatos(real_title)
    
    num_word_reals=len(lista_c_r)
        
    lista_almacen_real = []
    print('Caracteres eliminados')
    
    #Calculo de la frecuencia de las palabras
    frecuenciaPalab_real = []
    i = 0
    while len(lista_c_r) >= 1:
        print(i)
        contador = lista_c_r.count(lista_c_r[0])
        frecuenciaPalab_real.append(contador)
        lista_almacen_real.append(lista_c_r[0])
        while contador > 0:
            lista_c_r.remove(lista_c_r[0])
            contador -= 1
        i += 1
            
            
    lista_frec_word_real=list(zip(lista_almacen_real,frecuenciaPalab_real))

    #print(lista_frec_word_real)
    print("frecuencia1")
    
    #Calculo de la Clase Fake
    #Pasamos los strings a lista
    lista_c_n = filtradorDatos(fake_title)

    num_word_fake=len(lista_c_n)
    
    print("replace2")
    #print(lista_c_n)

    
    #Calculo de la frecuencia de las palabras
    frecuenciaPalab_fake = []
    lista_almacen_fake = []
                             
    while len(lista_c_n) >= 1:
        print(i)
        contador = lista_c_n.count(lista_c_n[0])
        frecuenciaPalab_fake.append(contador)
        lista_almacen_fake.append(lista_c_n[0])
        while contador > 0:
            lista_c_n.remove(lista_c_n[0])
            contador -= 1
        i += 1
            
        
    lista_frec_word_fake=list(zip(lista_almacen_fake,frecuenciaPalab_fake))
    print("frecuencia2")
    #print(lista_frec_word_real)
    #print(lista_frec_word_fake)


    suma_real = 0
    suma_fake = 0
    for i in lista_frec_word_real:
        suma_real += i[1]
        
    for i in lista_frec_word_fake:
        suma_fake += i[1]

    print(suma_fake)
    print(suma_real)


  
    print("lista2")

    #En estas lineas creamos la lista del vocabulario 
    list_vocabulario=[]

    for voc in lista_frec_word_real:
        for word in voc:
            if type(word) != int:
                if word not in list_vocabulario:
                    list_vocabulario.append(word)
    for voc in lista_frec_word_fake:
        for word in voc:
            if type(word) != int:
                if word not in list_vocabulario:
                    list_vocabulario.append(word)
            

    print(len(list_vocabulario), "  list_vocabulario")
    print(len(lista_frec_word_real), "  lista_frec_word_real")
    print(len(lista_frec_word_fake), "  lista_frec_word_fake")
    print(num_word_fake, "  lista_c_n")
    print(num_word_reals, "  lista_c_r") 
    print("vocabulario1")

    #Calculo de probabilidades
    num_vocabulario=len(list_vocabulario)
    
    #Calculo de probabilidades reals
    #1º Calculamos las probabilidades de las palabras que se encuentran en la lista real
    lista_prob_reals=[]
    lista_word_prob_reals=[]
    for preals in lista_frec_word_real:
        for pfrec in preals:
            if type(pfrec) == int:
                lista_prob_reals.append((pfrec+1)/(num_word_reals+num_vocabulario))
            else:
                lista_word_prob_reals.append(pfrec) 

    print("posibilidades1")
    
    #2º Calculamos las probabilidades de las palabras fakes que no están en la lista real
    list_fake_en_real=[]

    for voc in lista_frec_word_real:
        for word in voc:
            if type(word) != int:
                if word not in list_fake_en_real:
                    list_fake_en_real.append(word)
    for voc in lista_frec_word_fake:
        for word in voc:
            if type(word) != int:
                if word not in list_fake_en_real:                
                    lista_prob_reals.append((0+1)/(num_word_reals+num_vocabulario))            
                    lista_word_prob_reals.append(word) 
                    
    lista_pares_prob_reals=list(zip(lista_word_prob_reals,lista_prob_reals))
    print("posibilidades2")
    print(len(lista_pares_prob_reals))

    df_prob_real=pd.DataFrame(lista_pares_prob_reals,columns = ['Words' , 'Probability'])
    #print(df_prob_real.head())
    df_prob_real.to_json (r'C:\Users\kike\Desktop\DefakingNews\defaking-news\Algoritmo\Real_Probability.json',orient='index')


    #Calculo de probabilidades fake
    #1º Calculamos las probabilidades de las palabras real que se encuentran en la lista fake
    lista_prob_fake=[]
    lista_word_prob_fake=[]
    for pfake in lista_frec_word_real:
        for pfrec in pfake:
            if type(pfrec) == int:
                lista_prob_fake.append((pfrec+1)/(num_word_fake+num_vocabulario))
            else:
                lista_word_prob_fake.append(pfrec)
    #2º Calculamos las probabilidades de las palabras reals que no están en la lista fake
    list_real_en_fake=[]

    for voc in lista_frec_word_real:
        for word in voc:
            if type(word) != int:
                if word not in list_real_en_fake:
                    list_real_en_fake.append(word)
    for voc in lista_frec_word_fake:
        for word in voc:
            if type(word) != int:
                if word not in list_real_en_fake:                
                    lista_prob_fake.append((0+1)/(num_word_fake+num_vocabulario))
                    lista_word_prob_fake.append(word)
    lista_pares_prob_fake=list(zip(lista_word_prob_fake,lista_prob_fake))


    df_prob_fake=pd.DataFrame(lista_pares_prob_fake,columns = ['Words' , 'Probability'])
    #print(df_prob_fake.head())
    df_prob_fake.to_json (r'C:\Users\kike\Desktop\DefakingNews\defaking-news\Algoritmo\Fake_Probability.json',orient='index')
    return print('Calculo Probabilidades Se Ha Procesado Con Exito')


